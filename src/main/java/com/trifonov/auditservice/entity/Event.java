package com.trifonov.auditservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "app_event")
public class Event implements Serializable {

    @Id
    private String id;

    @Column(name = "title", nullable = false)
    @NotBlank(message = "String is null or empty")
    private String title;

    @Column(name = "description", nullable = false)
    @NotBlank(message = "String is null or empty")
    private String description;

    @Column(name = "create_date_time")
    @NotNull(message = "LocalDateTime is null")
    private LocalDateTime createDateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }
}
