package com.trifonov.auditservice.service;

import com.trifonov.auditservice.entity.Event;
import com.trifonov.auditservice.repository.EventRepository;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class EventService {

    private final EventRepository eventRepository;
    private final AmqpTemplate amqpTemplate;

    @Autowired
    public EventService(EventRepository eventRepository, AmqpTemplate amqpTemplate) {
        this.eventRepository = eventRepository;
        this.amqpTemplate = amqpTemplate;
    }

    public void create(final Event event) {
        amqpTemplate.convertAndSend("queueEvent", event);
    }

    public List<Event> findAllByCreateDateTimeBetween(final LocalDateTime beginDate, final LocalDateTime endDate) {
        return eventRepository.findAllByCreateDateTimeBetween(beginDate, endDate);
    }
    
}
