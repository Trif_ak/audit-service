package com.trifonov.auditservice.listener;

import com.trifonov.auditservice.entity.Event;
import com.trifonov.auditservice.repository.EventRepository;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@EnableRabbit
public class RabbitMqListener {

    private final EventRepository eventRepository;

    @Autowired
    public RabbitMqListener(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Transactional
    @RabbitListener(queues = "queueEvent")
    public void processQueueEvent(final Event event) {
        eventRepository.save(event);
    }

}
