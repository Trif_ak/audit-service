# AUDIT-SERVICE

## TECHNOLOGIES

REST, BEAN VALIDATION, JPA, AMQP.

## SOFTWARE

- Java 8
- Spring Boot
- Hibernate
- Swagger
- Maven 4
- H2 Database (in-memory)
- RabbitMQ


## INFORMATION

- Документация веб-сервиса:

```bash
http://localhost:8080/swagger-ui.html
```

- Админка RabbitMQ:

```bash
http://localhost:15672/
```

- Консоль H2 DB

```bash
http://localhost:8081/h2-console
```

>Приложение разворачивается на порте 8080
>
>Для корректной работы приложения нужно развернуть RabbitMQ на порте 15672.

## DEVELOPER
  
- Trifonov Ilya <Trifakk@gmail.com>  
